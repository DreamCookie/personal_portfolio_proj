from django.db import models

class Blog(models.Model):
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=500)
    image = models.ImageField(upload_to='blog/images/', blank = True)
    url = models.URLField(blank=True)
    date = models.DateField()
